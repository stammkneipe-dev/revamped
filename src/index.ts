import { Elysia, t } from "elysia";

const imageDir = Bun.env.IMAGE_DIR ?? 'images';

const app = new Elysia()
  .get("/:id", ({ params: { id } }) => Bun.file(`${imageDir}/${id}`))
  .post("/", async ({ body, set }) => {
    const fileType = body.file.type.split('/');
    const fileClass = fileType[0];
    if (fileClass !== 'image') {
      set.status = 415;
      throw new Error('Only Images are allowed');
    }
    const fileEnding = fileType[1];
    const uuid = crypto.randomUUID();
    const response = `${uuid}.${fileEnding}`
    Bun.write(`${imageDir}/${response}`, body.file);
    return { 'fileName': response }
  }, {
      type: "multipart/form-data",
      body: t.Object({
        file: t.File({
          maxSize: "10m",
        }),
      }),
      error({code, error}) {
        switch (code) {
          case 'VALIDATION':
            console.log(error.message);
            return error.validator.Errors(error.value).First().message
        }
        return error.message;
      }
    })
  .listen(3000);

console.log(
  `🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`
);
