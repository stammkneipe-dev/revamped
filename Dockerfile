FROM oven/bun:1 as builder

WORKDIR /app

COPY package.json .
COPY bun.lockb .

RUN bun install --production

# ? -------------------------
FROM cgr.dev/chainguard/bun:latest

WORKDIR /app

COPY --from=builder /app/node_modules node_modules

COPY src src
COPY tsconfig.json .

ENV NODE_ENV production
CMD ["src/index.ts"]

EXPOSE 3000